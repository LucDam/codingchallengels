﻿/*
 * Refactorear la clase para respetar principios de programación orientada a objetos. Qué pasa si debemos soportar un nuevo idioma para los reportes, o
 * agregar más formas geométricas?
 *
 * Se puede hacer cualquier cambio que se crea necesario tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Trapecio/Rectangulo, agregar otro idioma a reporting.
 * */

using CodingChallenge.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;

namespace CodingChallenge.Data.Classes
{
    public class FormaGeometrica
    {
        private class EstructuraReporte
        {
            public int Cantidad { get; set; }
            public decimal AreaTotal { get; set; }
            public decimal PerimetroTotal { get; set; }
            public Type Tipo { get; set; }
        }

        public static string Imprimir(IFigura[] formas)
        {
            var sb = new StringBuilder();
            ResourceManager recursoIdioma = new ResourceManager("CodingChallenge.Data.RecursoIdioma.IdiomaReporte", 
                typeof(FormaGeometrica).Assembly); 

            if (!formas.Any())
            {
                sb.Append($"<h1>{recursoIdioma.GetString("Título_Vacio")}</h1>");
            }
            else
            {
                // HEADER
                sb.Append($"<h1>{recursoIdioma.GetString("Título")}</h1>");

                //Body
                List<EstructuraReporte> dataFormas = new List<EstructuraReporte>();

                foreach (var tipo in formas.GroupBy(f => f.GetType()).Select(s => s.Key))
                {
                    dataFormas.Add(new EstructuraReporte
                    {
                        AreaTotal = formas.Where(a => a.GetType().Equals(tipo)).Sum(s => s.CalcularArea()),
                        PerimetroTotal = formas.Where(a => a.GetType().Equals(tipo)).Sum(s => s.CalcularPerimetro()),
                        Cantidad = formas.Where(a => a.GetType().Equals(tipo)).Count(),
                        Tipo = tipo
                    });
                    sb.Append(ObtenerLinea(dataFormas.LastOrDefault(), recursoIdioma));
                }

                // FOOTER
                var formasTotales = dataFormas.Sum(s => s.Cantidad);
                var perimetroTotal = dataFormas.Sum(x => x.PerimetroTotal).ToString("#.##");
                var areaTotal = dataFormas.Sum(x => x.AreaTotal).ToString("#.##");
                sb.Append("TOTAL:<br/>");
                sb.Append($"{formasTotales} {recursoIdioma.GetString("Formas")} ");
                sb.Append($"{recursoIdioma.GetString("Perimetro")} {perimetroTotal} ");
                sb.Append($"{recursoIdioma.GetString("Area")} {areaTotal}");
            }
            return sb.ToString();
        }

        private static string ObtenerLinea(EstructuraReporte datosReporte, ResourceManager recursoIdioma)
        {
            if (datosReporte.Cantidad > 0)
            {
                var areaTotal = datosReporte.AreaTotal.ToString("#.##");
                var perimetroTotal = datosReporte.PerimetroTotal.ToString("#.##");
                var nombreFormaGeometrica = datosReporte.Cantidad > 1 ? recursoIdioma.GetString(datosReporte.Tipo.Name + "_plural") : recursoIdioma.GetString(datosReporte.Tipo.Name + "_singular");
                return $"{datosReporte.Cantidad} {nombreFormaGeometrica} | {recursoIdioma.GetString("Area")} {areaTotal} | {recursoIdioma.GetString("Perimetro")} {perimetroTotal} <br/>";
            }
            return string.Empty;
        }
    }
}
