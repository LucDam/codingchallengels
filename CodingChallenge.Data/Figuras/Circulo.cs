﻿using CodingChallenge.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Figuras
{
    public class Circulo : IFigura
    {
        private decimal diametro { get; set; }

        public Circulo(decimal d)
        {
            diametro = d;
        }

        public decimal CalcularArea()
        {
            return (decimal)Math.PI * (diametro / 2) * (diametro / 2);
        }

        public decimal CalcularPerimetro()
        {
            return (decimal)Math.PI * diametro;
        }
    }
}
