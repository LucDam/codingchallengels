﻿using CodingChallenge.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Figuras
{
    public class TrapecioRectangulo : IFigura
    {
        private decimal _lado_A { get; set; }
        private decimal _lado_B { get; set; }
        private decimal _lado_C { get; set; }
        private decimal _lado_D { get; set; }

        public TrapecioRectangulo(decimal lado_a, decimal lado_b, decimal lado_C, decimal lado_d)
        {
            _lado_A = lado_a;
            _lado_B = lado_b;
            _lado_C = lado_C;
            _lado_D = lado_d;
        }

        public decimal CalcularArea()
        {
            return _lado_C * ((_lado_A + _lado_B) / 2);
        }

        public decimal CalcularPerimetro()
        {
            return _lado_A + _lado_B + _lado_C + _lado_D;
        }
    }
}
