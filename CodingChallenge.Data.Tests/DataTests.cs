﻿using System;
using System.Collections.Generic;
using CodingChallenge.Data.Classes;
using CodingChallenge.Data.Figuras;
using CodingChallenge.Data.Interfaces;
using NUnit.Framework;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVacia()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
             new System.Globalization.CultureInfo("es-AR");
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>",
                FormaGeometrica.Imprimir(new IFigura[] { }));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
               new System.Globalization.CultureInfo("en-US");

            Assert.AreEqual("<h1>Empty list of shapes!</h1>",
                FormaGeometrica.Imprimir(new IFigura[] { }));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
                new System.Globalization.CultureInfo("es-AR");

            var cuadrado = new IFigura[] { new Cuadrado(5) };

            var resumen = FormaGeometrica.Imprimir(cuadrado);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perimetro 20 <br/>TOTAL:<br/>1 formas Perimetro 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
              new System.Globalization.CultureInfo("en-US");

            var cuadrados = new IFigura[]{new Cuadrado(5), new Cuadrado(1), new Cuadrado(3)};

            var resumen = FormaGeometrica.Imprimir(cuadrados);

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
                   new System.Globalization.CultureInfo("en-US");

            var formas = new IFigura[]
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = FormaGeometrica.Imprimir(formas);

            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>TOTAL:<br/>7 shapes Perimeter 97,66 Area 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
             new System.Globalization.CultureInfo("es-AR");

            var formas = new IFigura[]
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = FormaGeometrica.Imprimir(formas);

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perimetro 28 <br/>2 Círculos | Area 13,01 | Perimetro 18,06 <br/>3 Triángulos | Area 49,64 | Perimetro 51,6 <br/>TOTAL:<br/>7 formas Perimetro 97,66 Area 91,65",
                resumen);
        }

        //New TestCase Germany - Trapecio/Rectangulo:

        [TestCase]
        public void TestResumenListaVaciaFormasEnAleman()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
               new System.Globalization.CultureInfo("de-DE");

            Assert.AreEqual("<h1>Leere Liste der Formen!</h1>",
                FormaGeometrica.Imprimir(new IFigura[] { }));
        }

        [TestCase]
        public void TestResumenListaConUnTrapecio()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
               new System.Globalization.CultureInfo("de-DE");

            var cuadrado = new IFigura[] { new TrapecioRectangulo(3, 5, 6, 8) };

            var resumen = FormaGeometrica.Imprimir(cuadrado);

            Assert.AreEqual("<h1>Formularbericht</h1>1 Trapez | Bereich 24 | Umfang 22 <br/>TOTAL:<br/>1 Formen Umfang 22 Bereich 24", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTrapecio()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
               new System.Globalization.CultureInfo("de-DE");

            var cuadrados = new IFigura[] 
            {
                new TrapecioRectangulo(2, 3, 4, 5),
                new TrapecioRectangulo(7, 3, 5, 7),
                new TrapecioRectangulo(3, 6, 9, 8)
            };

            var resumen = FormaGeometrica.Imprimir(cuadrados);

            Assert.AreEqual("<h1>Formularbericht</h1>3 Trapezoide | Bereich 75,5 | Umfang 62 <br/>TOTAL:<br/>3 Formen Umfang 62 Bereich 75,5", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnAleman()
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
               new System.Globalization.CultureInfo("de-DE");

            var formas = new IFigura[]
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m),
                new TrapecioRectangulo(3, 6, 9, 8)
            };

            var resumen = FormaGeometrica.Imprimir(formas);

            Assert.AreEqual("<h1>Formularbericht</h1>2 Quadrate | Bereich 29 | Umfang 28 <br/>2 Kreise | Bereich 13,01 | Umfang 18,06 <br/>3 Dreiecke | Bereich 49,64 | Umfang 51,6 <br/>1 Trapez | Bereich 40,5 | Umfang 26 <br/>TOTAL:<br/>8 Formen Umfang 123,66 Bereich 132,15", resumen);
        }
    }
}
